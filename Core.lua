local addonName = ...

SBS = LibStub("AceAddon-3.0"):NewAddon(addonName, "AceConsole-3.0")
local AceGUI = LibStub("AceGUI-3.0")
SBS:RegisterChatCommand("sbs", "commands")

function SBS:PascalCaseToWords(s)
  local function split(char)
    return " " .. char
  end
  return s:gsub("[A-Z]", split):gsub("^.", string.upper):sub(2)
end

local friendlyAddonName = SBS:PascalCaseToWords(addonName)
local buff = ""

local options = {
  name = friendlyAddonName,
  type = 'group',
  args = {
    checkOnyOrNef = {
      type = 'toggle',
      name = 'Onyxia/Nefarian',
      desc = 'Should SBS check for Ony/Nef buff?',
      set = function(info, val) SBS.db.global.checkOnyOrNef = val end,
      get = function(info, val) return SBS.db.global.checkOnyOrNef end,
      width = 'full'
    },
    checkWCBOrSF = {
      type = 'toggle',
      name = "Warchief's Blessing/Songflower",
      desc = 'Should SBS check for WCB/SF buffs?',
      set = function(info, val) SBS.db.global.checkWCBOrSF = val end,
      get = function(info, val) return SBS.db.global.checkWCBOrSF end,
      width = 'full'
    },
    checkDMT = {
      type = 'toggle',
      name = 'Dire Maul Tribute',
      desc = 'Should SBS check for DMT buffs?',
      set = function(info, val) SBS.db.global.checkDMT = val end,
      get = function(info, val) return SBS.db.global.checkDMT end,
      width = 'full'
    }
  }
}

local defaults = {
  global = {
    checkOnyOrNef = true,
    checkWCBOrSF = true,
    checkDMT = true
  }
}

local optionsTable = LibStub("AceConfig-3.0"):RegisterOptionsTable(addonName, options)
local AceConfigDialog = LibStub("AceConfigDialog-3.0")
AceConfigDialog:AddToBlizOptions(addonName, friendlyAddonName)

function SBS:OnInitialize()
  self.db = LibStub("AceDB-3.0"):New(addonName .. "_DB", defaults)
end

function SBS:OnEnable()
  SBS:Print("Sekten Buff Snapshot -- Enabled")
end

function SBS:OnDisable()
  SBS:Print("Sekten Buff Snapshot -- Disabled")
end

function SBS:commands(msg)
  if (msg ~= "config" and msg ~= "help") and UnitInRaid("player") == nil then
    SBS:Print("You need to be in a raid!")
  elseif msg == "snapshot" then
    SBS:getSnapshot()
  elseif msg == "open" then
    SBS:showFrame(buff)
  elseif msg == "config" then
    InterfaceOptionsFrame_OpenToCategory(friendlyAddonName)
    -- Calling this method once only opens the "Game" section of the Interface Options. A second call gets us to "AddOns"
    InterfaceOptionsFrame_OpenToCategory(friendlyAddonName)
  elseif msg == "help" then
    SBS:PrintHelp()
  else
    SBS:PrintHelp()
  end
end

function SBS:PrintHelp()
  SBS:Print("Usage:\n/sbs snapshot -- creates a snapshot of the worldbuffs in the raid and displays window with tab-separated values.\n/sbs open -- shows the window again with the last snapshot taken in this session.\n/sbs config -- opens the addon options in the interface panel\n/sbs help -- shows this message")
end

function SBS:showFrame(buff)
  if buff=="" then
    SBS:Print("No snapshot taken. Use /sbs snapshot to take a snapshot.")
  else
    local frame = AceGUI:Create("Frame")
    frame:SetTitle("Sekten Buff Snapshot")
    frame:SetLayout("Flow")

    local editbox = AceGUI:Create("MultiLineEditBox")
    editbox:SetFullWidth(true)
    editbox:SetFullHeight(true)
    editbox:SetLabel("")
    editbox:DisableButton(1)
    editbox:SetText(buff)
    editbox:HighlightText()
    editbox:SetFocus()
    frame:AddChild(editbox)
  end
end

function SBS:getSnapshot()
  --	clear the buff var
  buff = ""
  -- 	insert headers, not sure if needed but it looks nice :P
  buff = "Name"

  if SBS.db.global.checkDMT == true then
    buff = buff .. "\tDMT"
  end
  if SBS.db.global.checkOnyOrNef == true then
    buff = buff .. "\tOny"
  end
  if SBS.db.global.checkWCBOrSF == true then
    buff = buff .. "\tWC/SF"
  end

  buff = buff .. "\n"

  local numMembers = GetNumGroupMembers();

  for i=1, numMembers do
    local pname = GetRaidRosterInfo(i);
    local ony = false
    local dmt = false
    local wcbsf = false
    for i=1,40 do
      local _, _, _, _, _, _, _, _, _, spellId = UnitBuff(pname,i)
      if SBS.db.global.checkDMT == true then
        if dmt ~= true and (spellId == 22820 or spellId == 22818 or spellId == 22817) then
          dmt = true
        end
      end
      if SBS.db.global.checkOnyOrNef == true then
        if ony ~= true and spellId == 22888 then
          ony = true
        end
      end
      if SBS.db.global.checkWCBOrSF == true then
        if wcbsf ~= true and (spellId == 16609 or spellId == 15366) then
          wcbsf = true
        end
      end
    end

    buff = buff .. pname .. "\t"

    local buffMatrix = {}

    if SBS.db.global.checkDMT == true then
      if dmt then
        table.insert(buffMatrix, "y")
      else
        table.insert(buffMatrix, "n")
      end
    end
    if SBS.db.global.checkOnyOrNef == true then
      if ony then
        table.insert(buffMatrix, "y")
      else
        table.insert(buffMatrix, "n")
      end
    end
    if SBS.db.global.checkWCBOrSF == true then
      if wcbsf then
        table.insert(buffMatrix, "y")
      else
        table.insert(buffMatrix, "n")
      end
    end

    for i = 1,#buffMatrix do
      local separator = ""
      if i == #buffMatrix then
        separator = "\n"
      else
        separator = "\t"
      end
      buff = buff .. buffMatrix[i] .. separator
    end

  end

  buff = buff .. "\nSnapshot taken " .. date("%a %b %d %H:%M:%S %Y")
  SBS:showFrame(buff)
end